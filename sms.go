package sms

import (
	"bytes"
	"fmt"
	"log"

	"ekaushalnsdcsms/config"
	"ekaushalnsdcsms/helper"
	"ekaushalnsdcsms/model"
	"net/http"
	"net/url"
	"strconv"
	"time"

	mgo "gopkg.in/mgo.v2"
)

var DB *mgo.Database
var Session *mgo.Session
var Mongo *mgo.DialInfo

func SendSMS(entity string, scenario string, mobileNumber int64, motp string, userId string, password string) {
	conf := config.ConfigReader
	uri := conf.GetString("MongoDBUrl")
	mongo, err := mgo.ParseURL(uri)
	s, err := mgo.Dial(uri)
	if err != nil {
		fmt.Printf("Can't connect to mongo, go error %v\n", err)
		panic(err.Error())
	}
	fmt.Println("Connected to", uri)
	Session = s
	Mongo = mongo

	databaseName := conf.GetString("DBNAME")

	DB = Session.DB(databaseName)
	defer s.Close()

	fmt.Println("SENDING sms")

	smsUserName := conf.GetString("smsUserName")
	smsPassword := conf.GetString("smsPassword")
	smsSenderID := conf.GetString("smsSenderId")

	template := GetSMSTemplate(entity, scenario, motp, userId, password)
	escaped := url.QueryEscape(template)
	mobile := strconv.FormatInt(mobileNumber, 10)
	surl := "http://www.smsjust.com/sms/user/urlsms.php?username=" + smsUserName + "&pass=" + smsPassword + "&senderid=" + smsSenderID + "&dest_mobileno=" + mobile + "&message=" + escaped + "&response=Y"

	fmt.Println("URL", surl)
	client := &http.Client{}
	var b []byte
	req, _ := http.NewRequest("POST", surl, bytes.NewBuffer(b))
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error in sending SMS", err)
	}
	var smsResponse model.SMSResponse
	dataByte := helper.StreamToByte(resp.Body)
	smsResponse.Date = time.Now()
	smsResponse.Response = string(dataByte)
	smsResponse.MobileNumber = mobile
	smsResponse.MessageBody = template

	inserterr := DB.C(model.CollectionSMSResponse).Insert(smsResponse)
	if inserterr != nil {
		log.Println("Error : failed to record response", inserterr)
	}
	defer resp.Body.Close()
}

func GetSMSTemplate(entity string, scenario string, motp string, userId string, password string) string {
	switch entity {
	case "User":
		switch scenario {
		case "otp":
			st := `Dear Applicant, Your OTP for Registration is ` + motp +
				`. This OTP is valid till 02 minutes. Do not share this OTP to anyone for security reasons.`
			return st
		case "ForgotPassword":
			st := `Dear Applicant, Your OTP for change password is ` + motp +
				`. This OTP is valid till 02 minutes. Do not share this OTP to anyone for security reasons.`
			return st
		}

	case "Training Partner":
		switch scenario {
		case "registration":
			st := `TP Login Details,Login Id: ` + userId +
				` Password: ` + password + `. Do not share these login details to anyone for security reasons.`
			return st
		case "otp":
			st := `OTP successfully generated for TP Registration. ` +
				`OTP : ` + motp
			return st
		case "spocchange":
			st := `OTP successfully generated for changing SPOC. ` +
				`OTP : ` + motp
			return st

		}
	case "Assessor":
		switch scenario {
		case "LinkAssessorToAssessmentAgency":
			st := `OTP successfully generated for linking assessor. ` +
				`OTP : ` + motp
			return st
		case "otp":
			st := `Dear Applicant, Your OTP for Registration is ` + motp +
				`. This OTP is valid till 02 minutes. Do not share this OTP to anyone for security reasons.`
			return st
		}
	case "Trainer":
		switch scenario {
		case "otp":
			st := `Dear Applicant, Your OTP for Registration is ` + motp +
				`. This OTP is valid till 02 minutes. Do not share this OTP to anyone for security reasons.`
			return st
		case "LinkTrainerToTP":
			st := `OTP successfully generated for linking trainer. ` +
				`OTP : ` + motp
			return st

		}

	}

	return ""
}
