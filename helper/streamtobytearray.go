package helper

import (
	"bytes"
	"io"
)

// StreamToByte : Converting stream io.Reader to []byte
func StreamToByte(stream io.Reader) []byte {
	buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.Bytes()
}
