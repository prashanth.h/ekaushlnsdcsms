# ekaushlnsdcsms

Package to send sms for ekaushalnsdc.

Update your toml file with proper configuration values whenever you import this package.

Use go get "gitlab.com/prashanth.h/ekaushlnsdcsms" to import the package.

Use SendSMS(entity string, scenario string, mobileNumber int64, motp string, userId string, password string) method to send sms