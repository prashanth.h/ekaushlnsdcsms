package model

import (
	"time"
)

const (
	CollectionSMSResponse = "smsresponse"
)

type SMSResponse struct {
	Date         time.Time `json:"date" bson:"date" form:"date"`
	Response     string    `json:"response" bson:"response" form:"response"`
	MobileNumber string    `json:"mobileNumber" bson:"mobileNumber" form:"mobileNumber"`
	MessageBody  string    `json:"messageBody" bson:"messageBody" form:"messageBody"`
}
